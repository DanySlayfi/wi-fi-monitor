package com.dany.slayfi.utils;

import java.io.IOException;
import java.io.InputStream;

public class Utils
{
  public static void bash(String command)
  {
    Process p;
    try
    {
      p = Runtime.getRuntime().exec(command);
      p.waitFor();
    }
    catch (IOException | InterruptedException ex)
    {
      ex.printStackTrace();
    }
  }

  public static String bashWithResult(String command)
  {
    StringBuilder output = new StringBuilder();
    InputStream reader = null;
    Process p;
    try
    {
      p = Runtime.getRuntime().exec(command);
      p.waitFor();

      reader = p.getInputStream();

      while (reader.available() > 0)
      {
        output.append((char) reader.read());
      }
    }
    catch (IOException | InterruptedException ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      if (reader != null)
      {
        try
        {
          reader.close();
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    }
    return output.toString();
  }

  public static void sleep(int delay)
  {
    try
    {
      Thread.sleep(delay);
    }
    catch (InterruptedException ex)
    {
      ex.printStackTrace();
    }
  }

  public static void killProcess(String processName)
  {
    if (processName == null)
    {
      throw new NullPointerException("Process name for kill is null");
    }
    String pid = bashWithResult("pidof " + processName);
    pid = pid.replaceAll("\n", "");
    if (!pid.isEmpty())
    {
      bash("kill -9 " + pid);
    }
  }
}
