package com.dany.slayfi.entity;

import com.dany.slayfi.utils.Utils;

import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Monitor
{
  private static Monitor monitor;

  private String firstSteamProcessName;
  private String secondStreamProcessName;
  private String recordProcessName;
  private String reportFileName;
  private String scriptName;
  private String mainPath;
  private boolean streamMode;
  private boolean isActive;
  private int pingInterval;

  private SimpleDateFormat format;
  private Thread thread;
  private URL urlForPing;

  private Monitor()
  {
  }

  public static Monitor getDefaultMonitor()
  {
    if (monitor == null)
    {
      monitor = new Monitor();
      monitor.init();
    }
    return monitor;
  }

  public void start()
  {
    boolean firstTest;
    boolean secondTest;

    while (isActive)
    {
      firstTest = !streamMode;
      secondTest = !streamMode;

      firstTest = pingGoogle();
      Utils.sleep(pingInterval);
      secondTest = pingGoogle();

      if (firstTest != streamMode && secondTest != streamMode)
      {
        writeEventOnLog();
        if (streamMode)
        {
          startRecord();
          streamMode = false;
        }
        else
        {
          startStream();
          streamMode = true;
        }
      }
    }
  }

  public void stop()
  {
    isActive = false;
  }

  private void startRecord()
  {
    Utils.killProcess(firstSteamProcessName);
    Utils.killProcess(secondStreamProcessName);
    thread = new Thread()
    {
      @Override
      public void run()
      {
        try
        {
          Process process = Runtime.getRuntime().exec("sh " + mainPath + scriptName + ".sh");
          process.waitFor();
        }
        catch (InterruptedException ex)
        {
          Utils.killProcess(recordProcessName);
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    };
    thread.start();
  }

  private void startStream()
  {
    thread.interrupt();
    thread = new Thread()
    {
      @Override
      public void run()
      {
        try
        {
          Process process = Runtime.getRuntime().exec(firstSteamProcessName);
          process.waitFor();
        }
        catch (InterruptedException ex)
        {
          Utils.killProcess(recordProcessName);
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    };
    thread.start();
  }

  private boolean pingGoogle()
  {
    try
    {
      urlForPing.openConnection().getContent();
    }
    catch (UnknownHostException e)
    {
      return false;
    }
    catch (IOException e)
    {
      return false;
    }
    return true;
  }

  private void writeEventOnLog()
  {
    StringBuilder builder = new StringBuilder();
    builder.append(streamMode ? "Соединение потеряно:          " : "Соединение восстановлено: ");
    builder.append(format.format(new Date()));
    builder.append('\n');

    try(FileWriter writer = new FileWriter(mainPath + "/reports/" + reportFileName, true))
    {
      writer.write(builder.toString());
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  private void init()
  {
    try
    {
      String currentUserName = Utils.bashWithResult("whoami");
      currentUserName = currentUserName.replaceAll("\n", "");
      mainPath = "/home/" + currentUserName + "/WiFi-Monitor/";
      format = new SimpleDateFormat("HH:mm:ss|dd.MM.yyyy");
      urlForPing = new URL("http://www.google.com");
      firstSteamProcessName = "ivideon-server";
      secondStreamProcessName = "videoserver";
      recordProcessName = "vlc";
      reportFileName = "report.txt";
      scriptName = "record";
      pingInterval = 5000;
      streamMode = true;
      isActive = true;
    }
    catch (MalformedURLException ex)
    {
      ex.printStackTrace();
    }
  }
}
