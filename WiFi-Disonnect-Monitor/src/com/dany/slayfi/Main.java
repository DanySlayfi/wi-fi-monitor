package com.dany.slayfi;

import com.dany.slayfi.entity.Monitor;

public class Main
{
  public static void main(String[] args) throws Exception
  {
    Monitor monitor = Monitor.getDefaultMonitor();
    monitor.start();
  }
}