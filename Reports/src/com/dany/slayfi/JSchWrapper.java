package com.dany.slayfi;

import com.jcraft.jsch.*;

import java.io.*;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

/**
 * Created by a on 2/6/16.
 */
public class JSchWrapper
{
  private ChannelSftp channel;

  public JSchWrapper()
  {
    init();
  }

  private void init()
  {
    JSch jSch = new JSch();
    Session session = null;
    try
    {
      session = jSch.getSession("dany", "192.168.0.104");
      session.setPort(22);
      session.setPassword("22883868");
      Properties properties = new Properties();
      properties.put("StrictHostKeyChecking", "no");
      session.setConfig(properties);
      session.connect();
      channel = (ChannelSftp) session.openChannel("sftp");
      channel.connect();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public String getTextFromFile(String file)
  {
    StringBuilder builder = new StringBuilder();
    InputStream inputStream = null;
    Reader reader = null;

    try
    {
      inputStream = channel.get(file);
      char[] buffer = new char[0x10000];
      reader = new InputStreamReader(inputStream, "UTF-8");
      int line = 0;
      do
      {
        line = reader.read(buffer, 0, buffer.length);
        if (line > 0)
        {
          builder.append(buffer, 0, line);
        }
      }
      while (line >= 0);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (reader != null && inputStream != null)
        {
          reader.close();
          inputStream.close();
        }
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }
    return builder.toString();
  }

  public void cd(String directory)
  {
    try
    {
      channel.cd(directory);
    }
    catch (SftpException ex)
    {
      ex.printStackTrace();
    }
  }

  public Vector ls(String directory)
  {
    Vector result = null;
    try
    {
      result = channel.ls(directory);
    }
    catch (SftpException ex)
    {
      ex.printStackTrace();
    }
    return result;
  }

  private InputStream getInputStrimFromFile(String file)
  {
    InputStream result = null;
    try
    {
      result = channel.get(file);
    }
    catch (SftpException ex)
    {
      ex.printStackTrace();
    }
    return result;
  }

  public void copyFilesToLocal(Set<String> files)
  {
    String currentUserName = bashWithResult("whoami");
    currentUserName = currentUserName.replaceAll("\n", "");
    InputStream inputStream = null;
    FileOutputStream outputStream = null;

    for (String file : files)
    {
      try
      {
        inputStream = getInputStrimFromFile(file);
        outputStream = new FileOutputStream("/home/" + currentUserName + "/Desktop/" + file);
        byte[] buffer = new byte[0x10000];
        int line = 0;
        do
        {
          line = inputStream.read(buffer, 0, buffer.length);
          if (line > 0)
          {
            outputStream.write(buffer, 0, line);
          }
        }
        while (line >= 0);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      finally
      {
        try
        {
          if (inputStream != null && outputStream != null)
          {
            inputStream.close();
            outputStream.close();
          }
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }
  }

  private String bashWithResult(String command)
  {
    StringBuilder output = new StringBuilder();
    InputStream reader = null;
    Process p;
    try
    {
      p = Runtime.getRuntime().exec(command);
      p.waitFor();

      reader = p.getInputStream();

      while (reader.available() > 0)
      {
        output.append((char) reader.read());
      }
    }
    catch (IOException | InterruptedException ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      if (reader != null)
      {
        try
        {
          reader.close();
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    }
    return output.toString();
  }
}
