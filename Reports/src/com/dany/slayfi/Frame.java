package com.dany.slayfi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame
{
  private JButton button;
  private JTextArea textArea;

  public Frame(String title, String report)
  {
    super(title);
    setLayout(new FlowLayout());
    textArea = new JTextArea(report);
    textArea.setSize(300, 480);
    add(textArea);
    setVisible(true);
    button = new JButton("Download video");
    if (report.isEmpty())
    {
      button.setEnabled(false);
      textArea.setText("No reports");
    }
    button.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent actionEvent)
      {

      }
    });
    add(button);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public JButton getButton()
  {
    return button;
  }
}
