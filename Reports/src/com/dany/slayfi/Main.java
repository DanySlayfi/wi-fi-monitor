package com.dany.slayfi;

import com.jcraft.jsch.ChannelSftp;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class Main
{

  public static void main(String[] args) throws Exception
  {
    final JSchWrapper jSch = new JSchWrapper();
    jSch.cd("/home/dany/WiFi-Monitor/reports/");
    Frame frm = new Frame("Reports", jSch.getTextFromFile("report.txt"));
    frm.setSize(350, 500);
    frm.getButton().addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent actionEvent)
      {
        jSch.cd("/home/dany/WiFi-Monitor/videos/");
        Set<String> filesToCopy = new HashSet<>();
        Vector vector = jSch.ls("/home/dany/WiFi-Monitor/videos/");
        for (int i = 0; i < vector.size(); i++)
        {

          if (".".equals(((ChannelSftp.LsEntry)vector.get(i)).getFilename()) ||
                  "..".equals(((ChannelSftp.LsEntry)vector.get(i)).getFilename()))
          {
            continue;
          }
          filesToCopy.add(((ChannelSftp.LsEntry)vector.get(i)).getFilename());
        }
        jSch.copyFilesToLocal(filesToCopy);
        JOptionPane.showMessageDialog(null, "Video is downloaded");
      }
    });
  }
}

